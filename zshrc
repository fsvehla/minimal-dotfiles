export PS1=$'\e[0;32m%n@%M\e[0m:\e[0;33m%~\e[0m$ '

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

[ -z "$HISTFILE" ] && HISTFILE="$HOME/.zsh_history"

setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt inc_append_history     # add commands to HISTFILE in order of execution

export HISTSIZE=10000 # maximum number of lines thate are kept in a session
export SAVEHIST=10000 # maximum number of lines that are kept in the history file

## End history

# Use modern completion system
autoload -Uz compinit
compinit