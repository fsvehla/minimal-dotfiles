set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp

set autoindent " Copy indentlevel to new line
set backspace=indent,eol,start
set cmdheight=2
set cursorline
set expandtab " Insert spaces instead of tabs
set hidden " Dont unload buffers on hide
set history=1000
set ignorecase " Ignore case when searching
set incsearch " Incremental searching
set laststatus=2 " Always show status line
set lsp=1 " linespacing
set number
set numberwidth=5
set scrolloff=3 " begin scrolling three lines before the fold
set shiftwidth=2 " used for cindent, >>, <<
set showmatch " briefly blink matching brackets
set tabstop=2 " interpret tabs as two spaces
set wildmenu
set wildmode=longest,list
set wrap

syntax on

set foldlevel=99 " disable folding